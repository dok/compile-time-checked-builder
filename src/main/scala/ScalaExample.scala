object ScalaExample extends App {

  import ScalaExample.Builder.is
  import scala.annotation.implicitNotFound

  trait NotConfigured
  trait Configured

  class Builder[A] private() {

    def configure(): Builder[Configured] = new Builder[Configured]

    def build()(implicit ev: Builder[A] is Builder[Configured]) = {
      println("It's work!")
    }
  }

  object Builder {

    @implicitNotFound("Builder is not configured!")
    sealed abstract class is[A, B]
    private val singleIs = new is[AnyRef, AnyRef] {}
    implicit def verifyTypes[A]: A is A = singleIs.asInstanceOf[is[A, A]]

    def apply(): Builder[NotConfigured] = {
      new Builder[NotConfigured]()
    }
  }

  Builder()
    .configure() // без вызова этого метода компилятор поругается:
    .build()     // Builder is not configured!
}


