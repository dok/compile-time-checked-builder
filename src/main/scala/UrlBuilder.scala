package scala

import java.net.URL

import scala.annotation.implicitNotFound

trait Defined
trait Undefined

class UrlBuilder[HasSchema, HasHost, HasFile] private(
  private var schema: String = "",
  private var host: String = "",
  private var port: Int = -1,
  private var file: String = "/"
) {

  def withSchema(schema: String): UrlBuilder[Defined, HasHost, HasFile] = {
    new UrlBuilder[Defined, HasHost, HasFile](schema, host, port, file)
  }

  def withHost(host: String): UrlBuilder[HasSchema, Defined, HasFile] = {
    new UrlBuilder[HasSchema, Defined, HasFile](schema, host, port, file)
  }

  def withPort(port: Int): UrlBuilder[HasSchema, HasHost, HasFile] = {
    new UrlBuilder[HasSchema, HasHost, HasFile](schema, host, port, file)
  }

  def withFile(file: String): UrlBuilder[HasSchema, HasHost, Defined] = {
    new UrlBuilder[HasSchema, HasHost, Defined](schema, host, port, file)
  }

  def build()(implicit ev: UrlBuilder[HasSchema, HasHost, HasFile] =:= UrlBuilder[Defined, Defined, Defined]): URL = {
    new URL(schema, host, file)
  }
}

object UrlBuilder {
  @implicitNotFound("Url configuration is not completed: HasSchema: ${HasSchema}, HasHost: ${HasHost}, HasFile: ${HasFile}")
  sealed abstract class is[A, B]
  private val singleIs = new is[AnyRef, AnyRef] {}
  implicit def verifyTypes[A]: A is A = singleIs.asInstanceOf[is[A, A]]

  def apply(): UrlBuilder[Undefined, Undefined, Undefined] = {
    new UrlBuilder[Undefined, Undefined, Undefined]()
  }
}

object Main extends App {

  val url = UrlBuilder()
    .withSchema("http")
    .withHost("localhost")
    .withFile("/")
    .build()

  println(url)
}