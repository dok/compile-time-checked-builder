public final class EqualTypes<L, R> {
    private EqualTypes() {}
    private static EqualTypes singletonEqualTypes = new EqualTypes();

    public static <T> EqualTypes<T, T> approve() {
        return (EqualTypes<T, T>) singletonEqualTypes;
    }
}