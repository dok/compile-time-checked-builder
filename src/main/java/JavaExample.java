public class JavaExample {

    interface NotConfigured {}

    interface Configured {}

    static class Builder<A> {
        static Builder<NotConfigured> init() {
            return new Builder<>();
        }

        private Builder() {}

        public Builder<Configured> configure() {
            return new Builder<>();
        }

        public void build(EqualTypes<Configured, A> approve) {
            System.out.println("It's work!");
        }
    }

    public static void main(String[] args) {
        Builder.init()
                .configure() // без вызова этого метода не скомпилируется
                .build(EqualTypes.approve());
//                .build(new EqualTypes());
    }
}
