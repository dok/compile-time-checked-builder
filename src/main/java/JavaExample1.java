public class JavaExample1 {
interface NotConfigured {}
interface Configured {}

static class Builder<IsConfigured> {
    static Builder<NotConfigured> init() {
        return new Builder<>();
    }
    private Builder() {}

    public Builder<Configured> configure() {
        return new Builder<>();
    }

    public <T extends Builder<Configured>> void build() {
        System.out.println("It's work!");
    }
}

public static void main(String[] args) {
    Builder.init()
            .configure()
            .<Builder<Configured>>build();
}
}
